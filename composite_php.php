<?php
//Component Interface
interface Employee
{
    public function getName();
    public function getSalary();
}
//Leaf Class
class Developer implements Employee
{
    private $name;
    private $salary;
    public function __construct($name, $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }
    public function getName(){return $this->name;}
    public function getSalary(){return $this->salary;}
}
//Composite Class
class Department implements Employee
{
    private $name;
    private $employees = [];
    public function __construct($name) { $this->name = $name; }
    public function addEmployee(Employee $employee) { $this->employees[] = $employee;}
    public function getName(){ return $this->name; }
    public function getSalary()
    {
        $totalSalary = 0;
        foreach ($this->employees as $employee) {
            $totalSalary += $employee->getSalary();
        }
        return $totalSalary;
    }
}
//Client Code
$developer1 = new Developer('Jon Doe', 5000);
$developer2 = new Developer('Jane Smith', 7000);
$designDepartment = new Department('Design Department');
$designDepartment->addEmployee($developer1);
$designDepartment->addEmployee($developer2);
$manager = new Developer('Manager', 8000);
$engineeringDepartment = new Department('Engineering Department');
$engineeringDepartment->addEmployee($designDepartment);
$engineeringDepartment->addEmployee($manager);
echo "Total salary of Engineering Department: " . $engineeringDepartment->getSalary();

